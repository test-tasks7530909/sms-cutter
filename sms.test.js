const sms = require('./sms');

const data = [
  {
    ex: 'Lorem ipsum dolor sit amet consectetur adipiscing elit', 
    toBe: ['Lorem ipsum dolor sit amet consectetur adipiscing elit']
  },
  {
    ex: `Lorem ipsum dolor sit amet consectetur adipiscing elit Nullam eleifend odio at magna pretium suscipit Nam commodo mauris felis ut suscipit velit efficitur eget Sed sit amet posuere risus`, 
    toBe: [
      `Lorem ipsum dolor sit amet consectetur adipiscing elit Nullam eleifend odio at magna pretium suscipit Nam commodo mauris felis ut 1/2`, 
      `suscipit velit efficitur eget Sed sit amet posuere risus 2/2`
    ]
  },
]

test('case 1', () => {
  expect(sms(data[0].ex)).toStrictEqual(data[0].toBe);  
});

test('case 2', () => {
  expect(sms(data[1].ex)).toStrictEqual(data[1].toBe);
});

