function sms(input) {  
  if (input.length <= 140) {
    return [input];
  }

  const matches = input.match(/(.{1,136})([' '|'. ']|$)/mg)

  return matches.map(
    (word, index)=>{
      return `${word.trim()} ${index+1}/${matches.length}`
    }
  )
}

module.exports = sms;
